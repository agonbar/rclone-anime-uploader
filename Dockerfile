FROM golang as builder

WORKDIR /go/src/gitea.adriangonzalezbarbosa.eu/agonbar/rclone-anime-uploader

COPY . .

RUN go get -d -v ./...

RUN GOOS=linux CGO_ENABLED=0 go build -o ./rclone-anime-uploader .

FROM alpine:latest

WORKDIR /app

COPY --from=builder /go/src/gitea.adriangonzalezbarbosa.eu/agonbar/rclone-anime-uploader/rclone-anime-uploader /app/
COPY --from=builder /go/src/gitea.adriangonzalezbarbosa.eu/agonbar/rclone-anime-uploader/config.yaml /

RUN chmod +x /app/rclone-anime-uploader

ENTRYPOINT /app/rclone-anime-uploader