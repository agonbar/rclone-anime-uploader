package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/fsnotify/fsnotify"

	_ "github.com/rclone/rclone/backend/drive"
	_ "github.com/rclone/rclone/backend/local"
	"github.com/rclone/rclone/fs"
	"github.com/rclone/rclone/fs/config"
	"github.com/rclone/rclone/fs/config/configfile"
	"github.com/rclone/rclone/fs/sync"

	"github.com/spf13/viper"
)

func main() {

	/***************
	** Parameters **
	***************/

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	// Folder to watch
	searchDir := viper.GetString("searchDir")
	destDir := viper.GetString("destDir")
	// Seconds to wait for more changes in files
	var waitTime = time.Duration(viper.GetInt("waitTime"))

	/********************
	** Inicializations **
	********************/

	// Instantiate a watcher in the folder
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// Create a timer to be able to wait until the copy is complete
	var timer1 = NewSecondsTimer(waitTime * time.Second)

	// Store an array with the names of the files changed
	changedFiles := make(map[string]bool)

	// List to be able to print the folder list later
	fileList := []string{}
	// CHeck if the monitored path exists
	if _, err := os.Stat(searchDir); os.IsNotExist(err) {
		panic(fmt.Errorf("The watched folder does not exist, please check the configuration."))
	}
	// Walk the monitored path
	filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		// If it is a dir, add to the watcher
		if f.IsDir() {
			// Add the folder to keep watch of
			err = watcher.Add(path)
			if err != nil {
				log.Fatal(err)
			}
			// Add to the previous list, for later use
			fileList = append(fileList, path)
		}
		return nil
	})

	/****************
	** EVENTS LOOP **
	****************/

	// This is the main loop, whaere we listen for all the events
	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				// If its a write, prepare the timer to launch a copy
				if event.Op&fsnotify.Write == fsnotify.Write {
					changedFiles[event.Name] = true
					if timer1.end.After(time.Now()) { // If timer didn't expire yet, reset it
						timer1.timer.Reset(waitTime * time.Second)
					} else { // Else recreate it
						timer1 = NewSecondsTimer(waitTime * time.Second)
					}
				}
				// If its a creation, check if its a folder, if so, add to watcher
				if event.Op&fsnotify.Create == fsnotify.Create {
					var f, err = os.Lstat(event.Name)
					if err != nil {
						panic(err)
					}
					if f.IsDir() {
						// Add the folder to keep watch of
						fmt.Println("ADDED: ", event.Name)
						err = watcher.Add(event.Name)
						if err != nil {
							log.Fatal(err)
						}
						// Add to the previous list, for later use
						fileList = append(fileList, event.Name)
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			// Once the timer is complete, launch a copy command
			case <-timer1.timer.C:
				copyToCloud(changedFiles, searchDir, destDir)
				changedFiles = make(map[string]bool)
			}
		}
	}()

	// Listen to the end of the main loop
	<-done
}

func copyToCloud(files map[string]bool, srcDir string, destDir string) bool {
	config.ConfigPath = viper.GetString("rcloneConfigPath")
	configfile.LoadConfig(context.Background())

	for key := range files {

		fdest, err := fs.NewFs(context.Background(), viper.GetString("rcloneRemoteName")+":"+strings.ReplaceAll(filepath.Dir(key), srcDir, destDir))
		if err != nil {
			log.Fatal(err)
		}

		fsource, err := fs.NewFs(context.Background(), filepath.Dir(key))
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Sync Folder Fired, Origin: " + fsource.String() + " , Destination: " + fdest.String())
		sync.CopyDir(context.Background(), fdest, fsource, true)
	}

	return true
}
