package main

import (
	"time"
)

// We create this new type of timer to be able to find if it has already expired since it was created
type SecondsTimer struct {
	timer *time.Timer
	end   time.Time
}

func (s SecondsTimer) Reset(t time.Duration) bool {
	return s.timer.Reset(t)
}

func NewSecondsTimer(t time.Duration) *SecondsTimer {
	return &SecondsTimer{time.NewTimer(t), time.Now().Add(t)}
}
