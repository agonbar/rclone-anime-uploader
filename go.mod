module gitea.adriangonzalezbarbosa.eu/agonbar/rclone-anime-uploader

go 1.16

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/rclone/rclone v1.55.0
	github.com/spf13/viper v1.7.1
)
